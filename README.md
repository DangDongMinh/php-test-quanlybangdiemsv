clone project https://gitlab.com/DangDongMinh/php-test-quanlybangdiemsv.git

composer install 

create new file .env

// run project
php artisan serve 

 setup database trong .env:<br/>
DB_CONNECTION=mysql<br/>
DB_HOST=127.0.0.1<br/>
DB_PORT=3306<br/>
DB_DATABASE=sinhvien<br/>
DB_USERNAME=root<br/>
DB_PASSWORD=

php artisan migrate 

// khởi tạo fake data bằng seeder

php artisan db:seed

// khởi tạo authencation (login logout)

php artisan make:auth 

//tài khoản login (admin@gmail.com       123456)